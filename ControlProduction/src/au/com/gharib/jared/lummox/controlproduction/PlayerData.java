package au.com.gharib.jared.lummox.controlproduction;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import au.com.gharib.jared.lummox.controlproduction.ControlCore.ControlAction;

public class PlayerData {
	
	private Player player;
	private File dataFile;
	private YamlConfiguration config;
	
	
	public PlayerData(Player player) {
		this.player   = player;
		this.dataFile = new File(ControlCore.getPlayerDataFolder(), player.getName() + ".yml");
		this.config   = YamlConfiguration.loadConfiguration(dataFile);
	}
	
	/**
	 * Gets amount of items 'actiond' by player
	 * ie  Gets amount of sand placed by player
	 * 
	 * @param a 	Action to get amount for
	 * @param item	Item to get amount for
	 * @return Amount 'actiond' to item
	 */
	public int getDone(ControlAction a, Material item) {
		try {
			int done = config.getConfigurationSection(a.toString()).getInt(item.getId() + "");
			return done;
		} catch (Exception ex) {
			return 0;
		}
	}
	
	/**
	 * Sets amount of items 'actiond' by player
	 * 
	 * @param a 		Action to set for
	 * @param item		Item to set done
	 * @param amount	New amount to set to. Paired with getDone() to increment values
	 */
	public void setDone(ControlAction a, Material item, int amount) {
		// action.item
		String path = String.format("%1!.%2!", a.toString(), item.getId() + "");
		config.set(path, amount);
		try {
			config.save(dataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Checks whether player can 'action' a block
	 * ie If a player can break a block
	 * 
	 * @param a 	The action to check
	 * @param item	Item to check
	 * @return		True if player has not gone above the limit for the block in the action
	 */
	public boolean canDo(ControlAction a, Material item) {
		return canDo(a, item, 0);
	}
	
	/**
	 * Checks whether player can 'action' a block
	 * ie If a player can break a block
	 * 
	 * @param a 				The action to check
	 * @param item				Item to check
	 * @param potentialAmount	Potential amount to compare to getDone(). Added to support the fact that 
	 * 							players can drop items in amounts greater than 1
	 * @return					True if player has not gone above the limit for the block in the action
	 */
	public boolean canDo(ControlAction a, Material item, int potentialAmount) {
		int limit = getLimit(a, item);
		int done  = getDone(a, item) + potentialAmount;
		
		return (done < limit);
	}
	
	/**
	 * Gets the amount a player can 'action' a block
	 * 
	 * @param a		Action to get
	 * @param item	Item to get
	 * @return The amount a player can do before going over the limit. ie Can drop 5 more items
	 */
	public int getCanDo(ControlAction a, Material item) {
		int limit = getLimit(a, item);
		int done  = getDone(a, item);
		
		return limit - done;
	}
	

	/**
	 * Gets the group the player is associated with
	 * 
	 * @return The name of player's group
	 */
	public String getGroup() {
		Set<String> allGroups = ControlCore.getInstance().getConfig().getConfigurationSection("groups").getKeys(false);
		for(String group : allGroups) {
			if(player.hasPermission("controlproduction.group." + group))
				return group;
		}
		
		return null;
	}
	
	/**
	 * Checks whether player is in a group
	 * 
	 * @return True if player is in a group
	 */
	public boolean isInGroup() {
		return !(player.hasPermission("controlproduction.nogroup") || getGroup() == null);
	}
	
	/**
	 * Gets the limit of a block to action 
	 * 
	 * @param a		Action to get
	 * @param item	Item to get
	 * @return	The limit of the block to action
	 */
	public int getLimit(ControlAction a, Material item) {
		ConfigurationSection group 	= ControlCore.getInstance().getConfig().getConfigurationSection("groups").getConfigurationSection(getGroup());
		int itemId 					= item.getId();
		
		try {
			int limit = group.getConfigurationSection(a.toString()).getInt(itemId + "");
			return limit;
		} catch (Exception ex) {
			return 0;
		}
	}

	
	/**
	 * Checks whether a limit exists for a block
	 * 
	 * @param a			Action to check
	 * @param item		Item to check under action
	 * @param group		Group to search in
	 * @return	True if limit exists for block in action for group
	 */
	public static boolean limitExists(ControlAction a, Material item, String group) {
		try {
			int limit;
			if(item == null) {
				limit = ControlCore.getInstance().getConfig().getConfigurationSection("groups")
							.getConfigurationSection(group)
							.getConfigurationSection(a.toString()).getKeys(false).toArray().length;
				return limit != 0;
			} else {
				limit = ControlCore.getInstance().getConfig().getConfigurationSection("groups")
						.getConfigurationSection(group)
						.getConfigurationSection(a.toString())
						.getInt(item.getId() + "");
				return true;
			}
			
			
		} catch (Exception ex) {
			return false;
		}
	}
	
	/**
	 * Resets statistics for players
	 * 
	 * @param name		Player to reset, null if all players
	 * @param action	Action to reset, null for all actions
	 * @param type		Item to reset, null for all items
	 */
	public static void reset(String name, ControlAction action, Material type) {
		for(File f : ControlCore.getPlayerDataFolder().listFiles()) {
			try {
				if(!f.getName().equalsIgnoreCase(name + ".yml"))
					continue;
			} catch (NullPointerException ex) {}
			
			YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
			
			// Loop through all actions
			for(ControlAction a : ControlAction.values()) {
				try {
					if(action != null && action != a)
						continue;
				} catch (NullPointerException ex) {}
				
				try {
					for(Object o : c.getConfigurationSection(a.toString()).getKeys(false)) {
						try {
							if(type.getId() != Integer.parseInt((String)o))
								continue;
						} catch (NullPointerException ex) {}
						
						c.set((String) o, 0);
						try {
							c.save(f);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				} catch (NullPointerException ex) {}
			}
		}
	}
	
}
