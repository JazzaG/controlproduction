package au.com.gharib.jared.lummox.controlproduction.listener;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import au.com.gharib.jared.lummox.controlproduction.ControlCore.ControlAction;
import au.com.gharib.jared.lummox.controlproduction.ControlCore;
import au.com.gharib.jared.lummox.controlproduction.Message;
import au.com.gharib.jared.lummox.controlproduction.PlayerData;

public class BlockListener implements Listener {
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e) {
		Player player   = e.getPlayer();
		PlayerData data = new PlayerData(player);
		
		// Check player is in a group
		if(!data.isInGroup())
			return;
		
		// Check player can break the block
		Material type = e.getBlock().getType();
		if(!data.canDo(ControlAction.BREAK, type)) {
			player.sendMessage(ControlCore.getMessage(Message.LIMIT_REACHED));
			e.setCancelled(true);
		} else {
			data.setDone(ControlAction.BREAK, type, 
					data.getDone(ControlAction.BREAK, type) + 1);
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e) {
		Player player   = e.getPlayer();
		PlayerData data = new PlayerData(player);
		
		// Check player is in a group
		if(!data.isInGroup())
			return;
		
		Material type = e.getBlock().getType();
		if(!data.canDo(ControlAction.PLACE, type)) {
			player.sendMessage(ControlCore.getMessage(Message.LIMIT_REACHED));
			e.setCancelled(true);
		} else {
			data.setDone(ControlAction.PLACE, type, 
					data.getDone(ControlAction.PLACE, type) + 1);
		}
	}

}
