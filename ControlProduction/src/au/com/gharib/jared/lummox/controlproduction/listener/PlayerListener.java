package au.com.gharib.jared.lummox.controlproduction.listener;

import java.io.File;
import java.io.IOException;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import au.com.gharib.jared.lummox.controlproduction.ControlCore;

public class PlayerListener implements Listener {

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		String name = e.getPlayer().getName();
		
		// Create data folder if not exists
		if(ControlCore.getPlayerData(name) == null) {
			try {
				new File(ControlCore.getPlayerDataFolder(), name + ".yml").createNewFile();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
}
