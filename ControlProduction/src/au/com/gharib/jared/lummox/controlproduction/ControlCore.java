package au.com.gharib.jared.lummox.controlproduction;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import au.com.gharib.jared.lummox.controlproduction.listener.*;
import au.com.gharib.jared.lummox.controlproduction.command.ControlProduction;

public class ControlCore extends JavaPlugin {
	private static ControlCore instance;	
	private static File playerDataFolder;
	private static File pluginDataFile; 
	private static File localeFile;
	
	public void onEnable() {
		instance = this;
		
		// Create all data folders
		saveDefaultConfig();
		playerDataFolder = new File(getDataFolder(), "player");
		pluginDataFile   = new File(getDataFolder(), "data.yml");
		localeFile       = new File(getDataFolder(), "locale.yml");
		playerDataFolder.mkdir();
		if(!localeFile.exists())
			saveResource("locale.yml", false);
		if(!pluginDataFile.exists())
			saveResource("data.yml", false);
		
		// Register commands
		getCommand("controlproduction").setExecutor(new ControlProduction());
		
		// Register listeners
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new BlockListener(), this);
		pm.registerEvents(new ItemListener(), this);
		pm.registerEvents(new PlayerListener(), this);
		
		// Begin timer
		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			public void run() {
				YamlConfiguration data = YamlConfiguration.loadConfiguration(pluginDataFile);
				int hours_passed = data.getInt("hours-passed");
				int hours_current;
				
				if(hours_passed >= getConfig().getInt("time-restriction")) {
					PlayerData.reset(null, null, null);
					hours_current = 0;
				} else {
					hours_current = hours_passed + 1;
				}
				
				data.set("hours-passed", hours_current);
				try {
					data.save(pluginDataFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}, 72000L, 72000L);
				
	}
	
	public static ControlCore getInstance() {
		return instance;
	}
		
	public static File getPlayerDataFolder() {
		return playerDataFolder;
	}
	
	public static YamlConfiguration getPlayerData(String player) {
		File playerData = new File(playerDataFolder, player + ".yml");
		if(playerData.exists())
			return YamlConfiguration.loadConfiguration(playerData);
		else
			return null;
	}
	
	public static String getMessage(Message m) {
		return YamlConfiguration.loadConfiguration(localeFile).getStringList("locale").get(m.getIndex());
	}
	
	
	public enum ControlAction {
		BREAK,
		CRAFT,
		DROP,
		PICKUP,
		PLACE,
		SMELT;
		 
		ControlAction() {}
	}

}
